class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    if (!ActiveRecord::Base.connection.tables.include?("articles"))
      create_table :articles do |t|
        t.string :title
        t.text :description

        t.timestamps
      end
    end
  end
end
